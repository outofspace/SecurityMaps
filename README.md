# SecurityMaps

Hacking, war, pandemics, climate and more 
--

## Environment and Climate
* https://www.windy.com/?52.474,13.337,5
* https://mapmaker.nationalgeographic.org/hGBQ57UFwqfZ8ypD5cjGDx/
* https://climate.nasa.gov/climate_resource_center/interactives/
* https://climate.nasa.gov/earth-now/?vs_name=visible_earth&dataset_id=852&group_id=53&animating=f&start=&end=
* https://www.impactlab.org/map/#usmeas=absolute&usyear=2020-2039&gmeas=absolute&gyear=2020-2039&tab=global&gvar=tas-DJF
* https://climatereanalyzer.org/wx/DailySummary/#t2
* https://earthobservatory.nasa.gov/global-maps
* http://climatemaps.romgens.com/
* https://crowtherlab.pageflow.io/cities-of-the-future-visualizing-climate-change-to-inspire-action#213121
* https://fitzlab.shinyapps.io/cityapp/
* https://earth.nullschool.net/#current/wind/isobaric/1000hPa/orthographic=-222.44,-25.30,454
* http://en.blitzortung.org/live_lightning_maps.php?map=16
* https://www.fireweatheravalanche.org
* https://www.fireweatheravalanche.org/fire/
* https://www.arcgis.com/home/webmap/viewer.html?webmap=df8bcc10430f48878b01c96e907a1fc3#!
* https://www.arcgis.com/home/webmap/viewer.html?webmap=8a1693523c5c4700ae897d0b50531504
* https://www.esri.com/en-us/disaster-response/disasters/wildfires
* https://fire.airnow.gov/
* https://firms.modaps.eosdis.nasa.gov/map/#d:2020-09-09..2020-09-10;@0.0,0.0,3z
* https://satellites.pro/
* https://zoom.earth/
* https://earth3dmap.com/earthquake-live-map/
* https://earthquake.usgs.gov/earthquakes/map/?extent=-46.67959,-145.37109&extent=72.07391,135.87891
* https://ds.iris.edu/seismon/index.phtml
* https://www.volcanodiscovery.com/earthquakes/today.html
* https://earthquaketrack.com/
* https://seismo.berkeley.edu/seismo.real.time.map.html
* https://www.emsc-csem.org/#2w
* https://quakes.globalincidentmap.com/
* http://flood.firetree.net/
* https://www.floodmap.net/
* http://globalfloodmap.org/
* https://msc.fema.gov/portal/search
* https://www.fmglobal.com/research-and-resources/nathaz-toolkit/flood-map

## Wars and conflicts
* https://conflicttr.com/
* https://liveuamap.com/
* https://syria.liveuamap.com/
* https://syriancivilwarmap.com/
* https://globaleventmap.org/
* https://www.globalincidentmap.com/
* https://www.cfr.org/global-conflict-tracker/
* https://chemicalweapons.gppi.net/data-portal/
* https://acleddata.com/dashboard/#/dashboard
* https://erickgn.github.io/mapafc/
* https://fogocruz.github.io/mapafc/
* https://www.google.com/maps/d/viewer?&mid=1CjpZqGvAV7WfAcPsjRvjoEzR10Y
* https://maps.clb.org.hk/?i18n_language=en_US&map=1&startDate=2011-01&endDate=2021-09&eventId=&keyword=&addressId=&parentAddressId=&address=&parentAddress=&industry=&parentIndustry=&industryName=
* https://apublica.org/mapadosconflitos/mapa
* https://mappingpoliceviolence.us/
* https://eyesonrussia.org/
* https://geoconfirmed.org/
* https://www.washingtoninstitute.org/islamicstateinteractivemap/#home

### Historical wars
* https://www.nationalarchives.gov.uk/
* https://www.thenewhumanitarian.org/maps-and-graphics/2017/04/04/updated-mapped-world-war

## Squats
* https://berlin-besetzt.de/


## Health / [Pan|En|Epi]demics / Sars-Cov-2 
* https://who.sprinklr.com/
* https://www.covidvisualizer.com/
* https://hgis.uw.edu/virus/
* https://news.google.com/covid19/map?hl=en-US&gl=US&ceid=US:en
* https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6
* https://www.arcgis.com/apps/opsdashboard/index.html#/85320e2ea5424dfaaa75ae62e5c06e61
* https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6
* https://coronavirus.jhu.edu/map.html
* https://www.healthmap.org/covid-19/
* https://www.healthmap.org/en/
* https://coronavirus-monitor.com/
* https://coronavirus.zone/
* https://coronavirus.app/
* https://nextstrain.org/ncov
* https://www.bing.com/covid
* https://sigageomarketing.com.br/coronavirus/
* https://especiais.g1.globo.com/bemestar/coronavirus/mapa-coronavirus/
* https://www.thenewhumanitarian.org/health/coronavirus
* https://experience.arcgis.com/experience/38efc69787a346959c931568bd9e2cc4
* https://covid.saude.gov.br/
* https://www.lagomdata.com.br/coronavirus
* https://labs.wesleycota.com/sarscov2/br/
* https://www.coronavirusnobrasil.org/
* https://brasil.io/covid19/
* https://dados.seplag.pe.gov.br/apps/corona.html#mapas
* https://mthbernardes.github.io/covid-brazil/

### COVID-19 Vaccine Tracker
* https://www.bloomberg.com/graphics/covid-vaccine-tracker-global-distribution/

## Wifi
* https://wigle.net/
* https://www.wifimap.io/860-berlin-free-wifi/map
* https://instabridge.com/free-wifi/
* https://openwifimap.net/#map?bbox=-17.978733095556155,-169.62890625,77.98904862437391,167.87109375
* https://wifidb.net/wifidb/opt/map.php?func=wifidbmap&labeled=0


## Internet / Hacking / Information Security / Pew Pew Maps
* https://atomicrbl.com/globe2/
* https://cybermap.kaspersky.com/
* https://torflow.uncharted.software/#?ML=6.328125,22.187404991398775,3
* https://www.spamhaustech.com/threat-map/
* https://threatmap.fortiguard.com/
* https://www.fireeye.com/cyber-map/threat-map.html
* https://threatmap.bitdefender.com/
* https://threatbutt.com/map/
* https://www.vanimpe.eu/pewpew/index.html
* https://globalsecuritymap.com/#
* https://threatmap.checkpoint.com/
* https://botnet-cd.trendmicro.com/
* https://www.akamai.com/us/en/resources/visualizing-akamai/
* https://www.digitalattackmap.com/#anim=1&color=0&country=ALL&list=0&time=18358&view=map
* https://map.lookingglasscyber.com/
* https://talosintelligence.com/fullpage_maps/pulse
* https://www.sophos.com/en-us/threat-center/threat-monitoring/threatdashboard.aspx
* https://attackmap.sonicwall.com/live-attack-map/
* https://horizon.netscout.com/?sidebar=close
* https://sicherheitstacho.eu/start/main
* https://www.immuniweb.com/radar/
* https://map.httpcs.com/
* https://community.blueliv.com/map/
* https://www.parsecuremap.com/#3/24.29/14.59
* https://isc.sans.edu/threatmap.html
* https://ocularwarfare.com/
* https://map.knowbe4.com/
* https://tormap.void.gr/
* https://ics-radar.shodan.io/
* https://exposure.shodan.io/#/
* https://atlasofsurveillance.org/
* https://bgpstream.com/
* https://bgpview.io/reports/countries#countries-heatmap
* https://www.infrapedia.com/app
* https://www.submarinecablemap.com/
* https://www.cfr.org/cyber-operations/
* https://embed.kumu.io/0b023bf1a971ba32510e86e8f1a38c38#apt-index
* https://crowdsec.net/log4j-tracker/
* https://qeeqbox.github.io/raven/index.html
* https://threatmap.opencti.net.br/
* https://www.akamai.com/internet-station/traffic-map
* https://livethreatmap.radware.com/
* https://www.spamhaus.com/threat-map/
* https://bis-threatmap.orange.ro/#
* https://www.imperva.com/cyber-threat-attack-map/

## Cell telephone system
### Security
* https://gsmmap.org/
### Cell Towers
* https://opencellid.org/


## Earth / Geographic / Street / Railway
* https://www.google.com/maps
* https://www.openstreetmap.org/#map=6/51.330/10.453
* https://earth.google.com/web/
* https://www.openrailwaymap.org/
* https://maps.metager.de/map
* https://www.bing.com/maps/
* https://www.qwant.com/maps
* https://apps.sentinel-hub.com/sentinel-playground
* https://livingatlas.arcgis.com/wayback/#active=42403&ext=-115.34120,36.03978,-115.25580,36.08821
* https://earth3dmap.com/
* https://wikimapia.org/
* https://apps.sentinel-hub.com/sentinel-playground/


### Brazilian toll calculation
* https://www.mapeia.com.br/
* https://qualp.com.br/
* https://rotasbrasil.com.br/

## Travel Regulations
* https://www.iatatravelcentre.com/world.php
* https://www.kayak.com/travel-restrictions

## Migration Policies
* https://mipex.eu/play/

## Satellites
* https://maps.esri.com/rc/sat2/index.html
* http://apps.agi.com/SatelliteViewer/
* http://stuffin.space/
* https://celestrak.com/cesium/orbit-viz.php?tle=/pub/TLE/catalog.txt&satcat=/pub/satcat.txt&referenceFrame=1
* https://james.darpinian.com/satellites/
* https://in-the-sky.org/satmap_worldmap.php
* https://in-the-sky.org/satmap_radar.php
* https://satmap.space/
* https://www.dishpointer.com/
* https://www.dishpointer.eu/
* https://www.satview.org/
* https://uphere.space/satellites/25544
* https://isstracker.pl/iss/przelot?adn=2459111.5751125&lat=37.09024&lng=-95.712891&z=4
* https://www.n2yo.com/
* https://app.spectator.earth/

## Airplanes
* https://www.flightradar24.com/27.32,-7.56/3
* https://planefinder.net/
* https://flightaware.com/live/
* https://flightaware.com/live/map
* https://globe.adsbexchange.com/

## Airports
* https://www.redemet.aer.mil.br/
* https://mrcl0wnlab.github.io/OSINT-Enum-de-infraestrutura-Aeroportos-Aerodrome/output_map_folium.html

## Ships
* https://shiptracker.live
* https://www.myshiptracking.com/
* https://www.vesselfinder.com/
* https://www.marinetraffic.com/
* https://www.cruisemapper.com/

